import { Injectable } from '@angular/core';
import { Observable, debounceTime, take, catchError, of, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { API_ROOT_PATH } from 'src/environments/environment';
import { Beer } from 'src/app/types/beer.type';
import { ErrorService } from '../handlers/error.service';

@Injectable({
  providedIn: 'root'
})
export class BeersService {

  constructor(private readonly http: HttpClient, private readonly errSrv: ErrorService) { }

  readonly BEERS_PATH = API_ROOT_PATH + 'beers'

  getByFood(food: string = '', page: number = 1, per_page: number = 25): Observable<Beer[]> {
    const params = { food, page, per_page };

    return this.http.get<Beer[]>(this.BEERS_PATH, {
      params: new HttpParams({ fromObject: params })
    }).pipe(take(1), debounceTime(10000))
  }
}
