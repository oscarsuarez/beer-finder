import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Beer } from 'src/app/types/beer.type';
import { API_ROOT_PATH } from 'src/environments/environment';

import { BeersService } from './beers.service';

describe('BeersService', () => {
  let service: BeersService;

  let expectedResult: Beer[] = []
  let httpController: HttpTestingController;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    service = TestBed.inject(BeersService);
    httpController = TestBed.inject(HttpTestingController);


    expectedResult = [
      { id: 1, description: 'desc1', name: 'name1', image_url: 'url1', tagline: 'tagLine1', food_pairing: ['food1', 'food2', 'food3'] },
      { id: 2, description: 'desc2', name: 'name2', image_url: 'url2', tagline: 'tagLine2', food_pairing: ['food1', 'food2', 'food3'] },
      { id: 3, description: 'desc3', name: 'name3', image_url: 'url3', tagline: 'tagLine3', food_pairing: ['food1', 'food2', 'food3'] },
      { id: 4, description: 'desc4', name: 'name4', image_url: 'url4', tagline: 'tagLine4', food_pairing: ['food1', 'food2', 'food3'] },
    ];
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return 4 beers', () => {
    service.getByFood('food', 1).subscribe(res => {
      expect(res.length).toBe(4)
    })

    const REQUEST = httpController.expectOne({
      method: 'GET',
      url: `${API_ROOT_PATH}beers?food=food&page=1&per_page=25`,
    });

    REQUEST.flush(expectedResult)
  })
});
