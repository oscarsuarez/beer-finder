import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor() { }

  showMessage(message: string): void {
    alert(message);
  }
}
