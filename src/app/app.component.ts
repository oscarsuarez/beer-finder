import { Component, OnInit } from '@angular/core';
import { debounceTime, finalize } from 'rxjs';
import { BeersService } from './services/api/beers.service';
import { ErrorService } from './services/handlers/error.service';
import { Beer } from './types/beer.type';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  beers: Beer[] = [];
  showSpinner: boolean = false;
  currentValue: string | null = null;

  // Pagination
  currentPage: number = 1;
  resultPerPage = 25;

  constructor(private readonly beersSrv: BeersService, private readonly errorSrv: ErrorService) { }

  ngOnInit(): void {
  }

  fetchBeers(value: string | null, page: number): void {
    this.currentValue = value === '' ? null : value;
    this.currentPage = page;
    this.showSpinner = !!value;
    this.beers = [];

    if (!value) {
      this.beers = [];
      return;
    }

    this.beersSrv.getByFood(value, page, this.resultPerPage).pipe(debounceTime(10000), finalize(() => this.showSpinner = false)).subscribe({
      next: (res) => this.beers = res,
      error: () => this.errorSrv.showMessage('Oops! an error has ocurred, try again')
    })
  }

  trackBeer(index: number, beer: Beer) {
    return beer ? beer.id : undefined;
  }
}
