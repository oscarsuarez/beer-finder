import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchInputComponent } from './search-input/search-input.component';

import { CardComponent } from './card/card.component';
import { SpinnerComponent } from './spinner/spinner.component';



@NgModule({
  declarations: [SearchInputComponent, SpinnerComponent, CardComponent],
  imports: [
    CommonModule
  ],
  exports: [SearchInputComponent, SpinnerComponent, CardComponent]
})
export class SharedModule { }
