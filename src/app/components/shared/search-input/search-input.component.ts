import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.css']
})
export class SearchInputComponent {

  @Input() delay: number = 0;
  @Input() placeholder: string = ''

  @Output() valueChange = new EventEmitter<string>();

  timer: ReturnType<typeof setTimeout> | undefined;

  onValueChange(event: any): void {

    if (this.timer) {
      clearTimeout(this.timer)
    }

    this.timer = setTimeout(() => {
      this.valueChange.emit(event.target.value);
    }, this.delay)


  }

}
