import { Component, Input, OnInit } from '@angular/core';
import { Beer } from 'src/app/types/beer.type';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  @Input() beer: Beer | undefined

  constructor() { }

  ngOnInit(): void {
  }

}
