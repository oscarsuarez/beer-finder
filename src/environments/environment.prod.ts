export const environment = {
  production: true
};

export const API_ROOT_PATH = 'https://api.punkapi.com/v2/';