// tailwind.config.js
module.exports = {
  mode: 'jit',
  content: ['./src/**/*.{html,ts}']
}